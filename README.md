# osrm-prepared-russia-data
Extracted, partitioned and customized russian data for [OSRM](https://github.com/Project-OSRM/osrm-backend).

## Install
1 Install docker, docker-compose and git lfs

2 Clone repo

3 Go to repo folder and execute
```
docker-compose up
```

Backend server start at localhost:5000, frontend at localhost:9966, for more info
see docker-compose.yml and OSRM docs

## Update data

```
sh ./prepare.sh
```

It will take at least 16 GB of memory. 
On a virtual server with 2GB of memory and a 3GHz processor and a swap in 20GB, the data preparation took three days.

