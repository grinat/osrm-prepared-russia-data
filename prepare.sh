#!/usr/bin/sh

docker-compose down

# create data folder
# cd / && mkdir data && chmod 0777 data && cd /data

wget http://download.geofabrik.de/russia-latest.osm.pbf

/bin/dd if=/dev/zero of=/var/swap.1 bs=1M count=20000
/sbin/mkswap /var/swap.1
chmod 0600 /var/swap.1
/sbin/swapon /var/swap.1

docker run -t -v $(pwd):/data osrm/osrm-backend osrm-extract -p /opt/car.lua /data/russia-latest.osm.pbf

docker run -t -v $(pwd):/data osrm/osrm-backend osrm-partition /data/russia-latest.osrm
docker run -t -v $(pwd):/data osrm/osrm-backend osrm-customize /data/russia-latest.osrm

echo "done!"

docker-compose up -d
